<?php

return [

    'slogan' => 'Engineering services in mechanical engineering and special-purpose machinery',
    'offerTitle' => 'Our offer',
    'offer' => 'Our offer in the engineering services market is the development of design documentation and, if necessary, the manufacture of samples of machines and mechanisms for agriculture, road construction, oil and mining enterprises, peat extraction, and special purpose machinery.',
    'expTitle' => 'Great experience',
    'exp' => 'Great experience of working at machine-building enterprises allows us to develop machines, mechanisms, nodes and high-level technical details.',
    'analyzTitle' => 'Analysis and optimization',
    'analyz' => 'When developing new equipment on the terms of reference of the Customer, we offer a patent search on this topic with an analysis of possible ways to optimize the parameters of the product designation and its functional compliance with the requirements of the Customer.',
    'modernTitle' => 'Model layout',
    'modern' => 'To create a modern design for the design of the ROC, we design a project with the specification of the technology for manufacturing parts and produce a model of the model on a scale with details as requested by the customer.',
    'why' => 'Our advantages',
    'amount' => 'Over one hundred projects',
    'exper' => '7 years experience',
    'quality' => 'European quality',
    'cList' => 'List of categories of our design office in this section',
    'pList' => 'List of services of our design office in this category',
    'get' => 'Order',
    'preview' => 'preview',
    'cancel' => 'cancel',
    'tip' => 'Please fill in the order form. We will contact you at the time specified.'
];