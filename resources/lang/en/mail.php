<?php

return [

    'title' => 'Contact Us',
    'name' => 'Name',
    'phone' => 'Phone',
    'email' => 'Email',
    'time' => 'Time to contact with you',
    'message' => 'Your message',
    'send' => 'Send'
];