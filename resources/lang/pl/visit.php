<?php

return [

    'title' => 'Biuro projektowe',
    'name' => 'Andron Jurij',
    'phone' => '+48 884 668 378',
    'email' => 'willow@willowby.pl',
    'adress' => 'ul. Klarysewska 57B lok.19',
    'adress2' => '02-936 Warszawa',
    'role' => 'Director'
];