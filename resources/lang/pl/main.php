<?php

return [

    'slogan' => 'Usługi inżynieryjne w zakresie inżynierii mechanicznej i maszyn specjalnego przeznaczenia',
    'offerTitle' => 'Nasza oferta',
    'offer' => 'Nasza oferta na rynku usług inżynierskich polega na opracowaniu dokumentacji projektowej oraz, jeśli to konieczne, produkcji próbek maszyn i mechanizmów dla rolnictwa, budownictwa drogowego, przemysłu naftowego i wydobywczego, wydobycia torfu i maszyn specjalnego przeznaczenia.',
    'expTitle' => 'Świetne doświadczenie',
    'exp' => 'Wielkie doświadczenie w pracy w przedsiębiorstwach budowy maszyn pozwala nam rozwijać maszyny, mechanizmy, węzły i wysokopoziomowe detale techniczne.',
    'analyzTitle' => 'Analiza i optymalizacja',
    'analyz' => 'Opracowując nowy sprzęt pod kątem wymagań Klienta, oferujemy wyszukiwanie patentów na ten temat wraz z analizą możliwych sposobów optymalizacji parametrów oznaczenia produktu i jego funkcjonalnej zgodności z wymaganiami Klienta.',
    'modernTitle' => 'Układ modelu',
    'modern' => 'Aby stworzyć nowoczesny projekt do projektowania ROC, projektujemy projekt ze specyfikacją technologii do produkcji części i produkujemy model modelu w skali z detalami wymaganymi przez klienta.',
    'why' => 'Dlaczego wybrać nas?',
    'amount' => 'Ponad sto projektów',
    'exper' => '7 lat doświadczenia',
    'quality' => 'Europejska jakość',
    'cList' => 'Lista kategorii naszego biura projektowego w tej sekcji',
    'pList' => 'Lista usług naszego biura projektowego w tej kategorii',
    'get' => 'zamówić',
    'preview' => 'podgląd',
    'cancel' => 'znieść',
    'tip' => 'Proszę wypełnić formularz zamówienia. Skontaktujemy się z Tobą w określonym czasie.'
];