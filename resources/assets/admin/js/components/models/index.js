import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from '../@common/commonLayout.vue'

export default [
    {
        path: '/models',
        component: Layout,
        props: {
            partName: 'Типы продуктов',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список типов продуктов',
                component: Table, 
                props: {
                    partName: 'Список типов продуктов',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание типа продукта',
                component: Create,
                props: {
                    partName: 'Добавить тип продукта',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование типа продукта',
                component: Edit, 
                props: {
                    partName: 'Редактирование типа продукта',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]