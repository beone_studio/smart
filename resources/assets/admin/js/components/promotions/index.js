import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from '../@common/commonLayout.vue'

export default [
    {
        path: '/promotions',
        component: Layout,
        props: {
            partName: 'Акции',
            icon: 'sort',
        },
        children: [
            {
                path: '',
                name: 'Список акций',
                component: Table,
                props: {
                    partName: 'Список акций',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create',
                name: 'Создание акции',
                component: Create,
                props: {
                    partName: 'Добавить акции',
                    icon: 'add',
                    showInMenu: true
                }
            },
            {
                path: ':id',
                name: 'Редактирование акции',
                component: Edit,
                props: {
                    partName: 'Редактирование акции',
                    icon: 'edit',
                    showInMenu: false
                }
            }
        ]
    }
]