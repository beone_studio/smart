import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from '../@common/commonLayout.vue'

export default [
    {
        path: '/sizes',
        component: Layout,
        props: {
            partName: 'Размеры',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список размеров',
                component: Table, 
                props: {
                    partName: 'Список размеров',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание размера',
                component: Create,
                props: {
                    partName: 'Добавить размер',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование размера',
                component: Edit, 
                props: {
                    partName: 'Редактирование размера',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]