import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from '../@common/commonLayout.vue'

export default [
    {
        path: '/cases',
        component: Layout,
        props: {
            partName: 'Тип чехлов',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список типов чехлов',
                component: Table, 
                props: {
                    partName: 'Список типов чехлов',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание типа чехла',
                component: Create,
                props: {
                    partName: 'Добавить тип чехла',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование типа чехла',
                component: Edit, 
                props: {
                    partName: 'Редактирование типа чехла',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]