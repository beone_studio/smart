import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from '../@common/commonLayout.vue'

export default [
    {
        path: '/series',
        component: Layout,
        props: {
            partName: 'Модели',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список моделей',
                component: Table, 
                props: {
                    partName: 'Список моделей',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание модели',
                component: Create,
                props: {
                    partName: 'Добавить модель',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование модели',
                component: Edit, 
                props: {
                    partName: 'Редактирование модели',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]