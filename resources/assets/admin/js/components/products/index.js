import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from '../@common/commonLayout.vue'

export default [
    {
        path: '/products',
        component: Layout,
        props: {
            partName: 'Список продуктов',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список продуктов',
                component: Table, 
                props: {
                    partName: 'Список продуктов',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание продукта',
                component: Create,
                props: {
                    partName: 'Добавить продукт',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование продукта',
                component: Edit, 
                props: {
                    partName: 'Редактирование продукта',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]