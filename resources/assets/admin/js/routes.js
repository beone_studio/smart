import categories from './components/categories';
import sizes from './components/sizes';
import models from './components/models';
import caseTypes from './components/caseTypes';
import series from './components/series';
import products from './components/products';
import promotions from './components/promotions';

export default [
    ...categories,
    ...sizes,
    ...models,
    ...caseTypes,
    ...series,
    ...products,
    ...promotions,
]