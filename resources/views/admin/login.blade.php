<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Администраторская</title>
    <link rel="stylesheet" href="{{ mix('/assets/admin/css/app.css') }}">
</head>
<body>
<div class="wrapper-page">
    <div class="panel">
        <div class="panel-body">
            <form class="form-horizontal m-t-20" method="post">
                {{ csrf_field() }}
                <span class="title">Панель управления</span>

                <span class="label">Логин</span>
                <div class="input-wrap">
                    <input name="username" type="text">
                </div>
                
                <span class="label">Пароль</span>
                <div class="input-wrap">
                    <input name="password" type="password">
                </div>
                
                
                <div class="actions">
                    <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit">Войти</button>
                </div>

                @if ($error)
                    <div class="form-group text-center m-t-40">
                        <div class="error">Неверный логин или пароль</div>
                    </div>
                @endif
            </form> 
        </div>                                    
    </div>
</div>
</body>
</html>