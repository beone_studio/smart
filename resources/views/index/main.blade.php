@extends('index.layout')

@section('head')
    <title>Smart</title>
@endsection

@section('content')

    <div id="page-wrapper">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="index.html" id="logo">Smart-choice</a></h1>
                    <hr/>
                    <p>Матрасы на любой вкус</p>
                </header>
                <footer>
                    <p>
                        Не знаете как выбрать матрас? Можете скачать файл.
                        <br>
                        В нем подробно описаны критерии выбора матраса.
                    </p>
                    <a data-popup-open="download" class="button">Скачать</a>
                </footer>
            </div>

            <!-- Nav -->
            <nav id="nav">
                <ul>
                    @foreach($models as $model)
                        <li><a href="#" class="nav-link" target="{{  $model->id  }}">{{  $model->name  }}</a></li>
                    @endforeach
                    @if(count($promotions))
                    <li><a href="#" class="nav-link" target="promotions">Акции</a></li>
                    @endif
                    <li><a href="#" class="nav-link" target="terms">Условия рассрочки</a></li>
                    <li><a href="#" class="nav-link" target="contact">Контакты</a></li>
                </ul>
            </nav>

        </div>

        <!-- Benefits -->
        <div class="wrapper style1">

            <section id="features" class="container special">
                <header>
                    <h2>Наши преимущества</h2>
                </header>
                <div class="row">
                    <article class="col-2 col-12-mobile special">
                        <a href="#" class="image featured icon"><span class="fa fa-calendar-check-o"></span></a>
                        <header>
                            <h4>Рассрочка до 12 месяцев без переплат</h4>
                        </header>
                    </article>

                    <article class="col-2 col-12-mobile special">
                        <a href="#" class="image featured icon"><span class="fa fa-credit-card"></span></a>
                        <header>
                            <h4>Мы принимаем карты рассрочки: Халва, Карта Покупок, Черепаха</h4>
                        </header>
                    </article>

                    <article class="col-2 col-12-mobile special">
                        <a href="#" class="image featured icon"><span class="fa fa-clock-o"></span></a>
                        <header>
                            <h4>При заказе до 14:00 - привезем матрас уже сегодня!</h4>
                        </header>
                    </article>

                    <article class="col-2 col-12-mobile special">
                        <a href="#" class="image featured icon"><span class="fa fa-truck"></span></a>
                        <header>
                            <h4>Вывезем Ваш старый матрас</h4>
                        </header>
                    </article>

                    <article class="col-2 col-12-mobile special">
                        <a href="#" class="image featured icon"><span class="fa fa-bed"></span></a>
                        <header>
                            <h4>Бесплатная доставка матраса до кровати</h4>
                        </header>
                    </article>

                    <article class="col-2 col-12-mobile special">
                        <a href="#" class="image featured icon"><span class="fa fa-gift"></span></a>
                        <header>
                            <h4>Без предоплаты + официальная гарантия</h4>
                        </header>
                    </article>
                </div>
            </section>

        </div>

        @if(count($promotions))
        <div class="wrapper style1" id="sectionpromotions">

            <section id="features" class="container special">
                <header>
                    <h2>Акции! Успей воспользоваться!</h2>
                </header>
                <br>
                <div class="row">
                    @foreach ($promotions as $promotion)
                        <article class="col-4 col-12-mobile special">
                            <a href="#" class="image featured"><img src="{{$promotion->image_url}}" alt="" /></a>
                            <header>
                                <h3>{{ $promotion->title }}</h3>
                            </header>
                            <p style="text-align: center">{{ $promotion->text }}</p>
                        </article>
                    @endforeach
                </div>
            </section>

        </div>
        @endif

    @foreach ($models as $model)
        <!-- Features -->
            <div id="section{{ $model->id }}" class="wrapper style1">
                <section id="features" class="container special">
                    <header>
                        <h2>{{  $model->name  }}</h2>
                        <div>{{ $model->description }}</div>
                    </header>
                    <div class="row">
                        <item-slider
                                :model-id="'{!! \json_encode($model->id) !!}'"
                                :sizes="{{ \json_encode($sizes) }}"
                                :case-types="{{ \json_encode($caseTypes) }}">
                        </item-slider>
                    </div>
                </section>

            </div>
    @endforeach

    <!-- Footer -->
        <div id="footer">
            <div class="container">
                <div id="sectionterms" class="row">

                    <!-- Tweets -->
                    <section class="col-4 col-12-mobile">
                        <header style="margin-bottom: 50px">
                            <h2 class="icon fa-money circled"><span class="label">Tweets</span></h2>
                            <h3>Способы оплаты</h3>
                        </header>
                        <ul class="divided">
                            <li>
                                <article class="tweet">
                                    <header>Наличные</header>
                                </article>
                            </li>
                            <li>
                                <article class="tweet">
                                    <header>Карта рассрочки Халва, Халва+, Черепаха, Карта покупок</header>
                                </article>
                            </li>
                            <li>
                                <article class="tweet">
                                    <header>Банковская карта</header>
                                </article>
                            </li>
                            <li>
                                <article class="tweet">
                                    <header>Через оформление рассрочки в ОАО "АСБ Беларусбанк"</header>
                                </article>
                            </li>
                        </ul>
                    </section>

                    <!-- Posts -->
                    <section class="col-4 col-12-mobile">
                        <header style="margin-bottom: 50px">
                            <h2 class="icon fa-file-text circled"><span class="label">Условия рассрочки</span></h2>
                            <h3>Условия рассрочки</h3>
                        </header>
                        <ul class="divided">
                            <li>
                                <article class="post stub">
                                    <header>
                                        Халва
                                    </header>
                                    <span class="timestamp">
                                        3 месяца на неакционные товары
                                        <br>
                                        2 месяца на акционные товары
                                    </span>
                                </article>
                            </li>
                            <li>
                                <article class="post stub">
                                    <header>
                                        Халва + (кэшбек 3%)
                                    </header>
                                </article>
                            </li>
                            <li>
                                <article class="post stub">
                                    <header>
                                        Черепаха (6 месяцев)
                                    </header>
                                </article>
                            </li>
                            <li>
                                <article class="post stub">
                                    <header>
                                        Карта покупок (6 месяцев)
                                    </header>
                                </article>
                            </li>
                            <li>
                                <article class="post stub">
                                    <header>
                                        ОАО "АСБ Беларусбанк" (интернет-банкинг) – до 12 месяцев
                                    </header>
                                    <span class="timestamp">
                                        Скоростное оформление рассрочки не выходя из дома (онлайн) в течение 1 часа в рабочее время Отдела продаж интернет-магазина Matras.Smart-Choice.by (только для держателей пластиковой карты ОАО "АСБ Беларусбанк")
                                    </span>
                                </article>
                            </li>
                            <li>
                                <article class="post stub">
                                    <header>
                                        ОАО "АСБ Беларусбанк" – до 12 месяцев
                                    </header>
                                    <span class="timestamp">
                                       Отличный вариант для тех, кто не хочет разбираться в онлайн оформлении и предпочитает лично посетить банк для оформления рассрочки (для держателей пластиковых карт других банков).
                                    </span>
                                </article>
                            </li>
                        </ul>
                    </section>

                    <!-- Photos -->
                    <section class="col-4 col-12-mobile">
                        <header style="margin-bottom: 50px">
                            <h2 class="icon fa-file circled"><span class="label">Photos</span></h2>
                            <h3>Сертификаты на продукцию</h3>
                        </header>
                        <div class="row gtr-25">
                            <div class="col-6">
                                <a href="#" class="image fit"><img src="/assets/index/img/sertificat.jpeg" alt=""/></a>
                            </div>
                            <div class="col-6">
                                <a href="#" class="image fit"><img src="/assets/index/img/sertificat.jpeg" alt=""/></a>
                            </div>
                            <div class="col-6">
                                <a href="#" class="image fit"><img src="/assets/index/img/sertificat.jpeg" alt=""/></a>
                            </div>
                            <div class="col-6">
                                <a href="#" class="image fit"><img src="/assets/index/img/sertificat.jpeg" alt=""/></a>
                            </div>
                        </div>
                    </section>

                </div>
                <hr/>
                <div class="row">
                    <div class="col-12">

                        <!-- Contact -->
                        <section id="sectioncontact" class="contact">
                            <header>
                                <h2 style="margin-bottom: 40px">Бесплатная консультация</h2>
                            </header>
                            <form method="post" action="/send">
                                <div class="row">
                                    <div class="col-6 col-12-mobile">
                                        <input type="text" name="user" placeholder="Ваше имя">
                                    </div>
                                    <div class="col-6  col-12-mobile">
                                        <input id="phoneMail"
                                               name="telephone"
                                               type="text"
                                               value="+375 (__) ___-__-__"
                                               pattern="^\+375(\s+)?\(?(17|25|29|33|44)\)?(\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$"
                                               required>
                                    </div>
                                    <div class="col-12">
                                        <button id="contact-form-button" class="large" type="submit">Заказать</button>
                                    </div>
                                </div>
                            </form>
                            <header>
                                <h2 style="margin-bottom: 30px">Ждем вашего звонка</h2>
                            </header>
                            <div class="row" style="margin-bottom: 80px">
                                <div class="col-6 col-12-mobile">
                                    <h3>+375(29) 366-07-72 (Velcom)</h3>
                                </div>
                                <div class="col-6 col-12-mobile">
                                    <h3>+375(29) 556-07-72 (MTC)</h3>
                                </div>
                            </div>
                            <header>
                                <h3 style="margin-bottom: 30px">Следите за нами в социальных сетях</h3>
                            </header>
                            <ul class="icons">
                                <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                                <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                                <li><a href="#" class="icon fa-vk"><span class="label">Vkontakte</span></a></li>
                            </ul>
                        </section>

                        <!-- Copyright -->
                        <div class="copyright">
                            <ul class="menu">
                                <li>&copy;ИП Барауля С.М. Все права защищены.</li>
                                <li>УНП 692106615 </li>
                                <li>Св. о регистрации №692106615 от 13.11.2018.</li>
                            </ul>
                        </div>
                        <div class="copyright">
                            <ul class="menu">
                                <li>Разработано: <a target="_blank" href="https://beone.by">beone studio</a></li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div data-popup="download" class="popup" style="display: none">
            <div class="popup-inner">
                <h3>Заполните, пожалуйста, эти поля, чтобы скачать файл</h3>
                <form class="send-email" method="post" action="/send">
                    <input style="display: none" type="text" id="product" name="product" value="-1">
                    <label for="userNameFile">Ваше имя</label>
                    <input id="userNameFile" name="user" type="text" placeholder="Ваше имя">
                    <label for="phoneFile">Телефон</label>
                    <input id="phoneFile"
                           name="telephone"
                           type="text"
                           value="+375 (__) ___-__-__"
                           pattern="^\+375(\s+)?\(?(17|25|29|33|44)\)?(\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$"
                           required>
                    <div class="actions">
                        <button class="close" data-popup-close="download">Закрыть</button>
                        <button id="download" type="submit" disabled>Скачать</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection