<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-RU" lang="ru-RU">
	<head>
		@yield('head')
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
		<link rel="stylesheet" href="/assets/index/css/animate.css">
		<link rel="stylesheet" href="{{ mix('/assets/index/css/app.css') }}">
		<style>
			button[disabled] {
				background: grey !important;
			}
		</style>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
		<script src="/assets/index/js/wow.min.js"></script>
		<script src="/assets/index/js/mask.js"></script>
		<script src="/assets/index/js/siema.min.js"></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129439141-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-129439141-1');
        </script>
        
		<noscript><link rel="stylesheet" href="/assets/index/css/noscript.css" /></noscript>
	</head>
	<body>
		<div id="app">
			@yield('content')
		</div>

		<script src="{{ mix('/assets/index/js/app.js') }}"></script>
		<script src="/assets/index/js/jquery.min.js"></script>
		<script src="/assets/index/js/jquery.dropotron.min.js"></script>
		<script src="/assets/index/js/jquery.scrolly.min.js"></script>
		<script src="/assets/index/js/jquery.scrollex.min.js"></script>
		<script src="/assets/index/js/browser.min.js"></script>
		<script src="/assets/index/js/breakpoints.min.js"></script>
		<script src="/assets/index/js/util.js"></script>
		<script src="/assets/index/js/main.js"></script>
		<script>
          $(function() {
            //----- OPEN
            $('[data-popup-open]').on('click', function(e)  {
              var targeted_popup_class = jQuery(this).attr('data-popup-open');
              $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

              e.preventDefault();
            });

            //----- CLOSE
            $('[data-popup]').on('click', function(e)  {
              var targeted_popup_class = jQuery(this).attr('data-popup');
              $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

              e.preventDefault();
            });

            $('[data-popup] > *').on('click', function(e)  {
              e.stopPropagation();
            });

            $('[data-popup-close]').on('click', function(e)  {
              var targeted_popup_class = jQuery(this).attr('data-popup-close');
              $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

              e.preventDefault();
            });
          });

          $('#download').on('click', function (e) {
			  download("/assets/index/kak_vibrat'_matras.pdf");
		  });

		  function download(filename) {
			  var element = document.createElement('a');

			  element.setAttribute('href', filename);
			  element.setAttribute('download', filename);

			  element.style.display = 'none';
			  document.body.appendChild(element);

			  element.click();

			  document.body.removeChild(element);
		  }

		  $('#userNameFile, #phoneFile').on('keyup', function (e) {
          	e.preventDefault();

          	var name = document.getElementById('userNameFile').value;
          	var phone = document.getElementById('phoneFile').value;
          	var validate = new RegExp('^\\+375(\\s+)?\\(?(17|25|29|33|44)\\)?(\\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$');

          	if (name && phone && validate.test(phone)) {
				$('#download').attr('disabled', false);
			} else {
				$('#download').attr('disabled', 'disabled');
			}
		  });

			$(".nav-link, .link").on('click', function (e) {
				e.preventDefault();

				var targetId = this.getAttribute('target');
				var y = $('#section' + targetId).offset().top;

				$("html,body").animate({
					scrollTop: y
				}, 800);
			});
			$(window).scroll(function (event) {
			  if (window.pageYOffset > window.innerHeight + 150) {
				$('#nav').addClass('fixed');
			  } else {
				$('#nav').removeClass('fixed');
			  }
			});
          window.onload = function() {
            MaskedInput({
              elm: document.getElementById('phoneFile'), // select only by id
              format: '+375 (__) ___-__-__',
              separator: '+375 ()-'
            });

            MaskedInput({
              elm: document.getElementById('phoneMail'), // select only by id
              format: '+375 (__) ___-__-__',
              separator: '+375 ()-'
            });
          };
		</script>
	</body>
</body>
</html>