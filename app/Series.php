<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 */
class Series extends Model
{
    protected $fillable = [
        'model_id',
        'name',
        'order',
        'slug',
        'weight',
        'weight_2',
        'feel_1',
        'feel_2',
        'feel_3',
        'feel_4',
        'height',
        'description',
        'image',
    ];

    protected $appends = [
        'image_url'
    ];

    public static function tableName(): string
    {
        return 'series';
    }

    public function productModel()
    {
        return $this->belongsTo(
            ProductModel::class,
            'model_id',
            'id'
        );
    }

    public function categories()
    {
        return $this
            ->belongsToMany(
                Category::class,
                'category_series',
                'series_id',
                'category_id'
            )
            ->orderBy(Category::tableName() . '.order');
    }

    public function products()
    {
        return $this->hasMany(
            Product::class,
            'series_id',
            'id'
        );
    }

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }
}
