<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = [
        'id',
        'title',
        'text',
        'image',
        'date_from',
        'date_to',
    ];

    protected $appends = [
        'image_url'
    ];

    public static function tableName(): string
    {
        return 'promotions';
    }

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }
}