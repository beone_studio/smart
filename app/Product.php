<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $size_id
 * @property string $name
 * @property Series $series
 */
class Product extends Model
{
    protected $fillable = [
        'price',
        'size_id',
        'series_id',
        'case_type_id',
        'order',
    ];

    public static function tableName(): string
    {
        return 'products';
    }

    public function size()
    {
        return $this->belongsTo(
            Size::class,
            'size_id',
            'id'
        );
    }

    public function caseType()
    {
        return $this->belongsTo(
            CaseType::class,
            'case_type_id',
            'id'
        );
    }

    public function series()
    {
        return $this->belongsTo(
            Series::class,
            'series_id',
            'id'
        );
    }
}
