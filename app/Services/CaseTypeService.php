<?php

namespace App\Services;

use App\CaseType;
use App\Repositories\CaseTypeRepository;

class CaseTypeService
{
    use ServiceTrait;

    public function __construct(CaseTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(array $requestData, CaseType $category = null)
    {
        if ($category instanceof CaseType) {
            return $category->update($requestData);
        }

        return CaseType::query()->create($requestData);
    }

    public function delete(int $id): void
    {
        $model = $this->getById($id);

        if (!$model->delete()) {
            throw new \RuntimeException('Can\'t delete model ' . $model->name);
        }
    }
}