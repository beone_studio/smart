<?php

namespace App\Services;

use App\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\Collection;

class CategoryService
{
    use ServiceTrait;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(array $requestData, Category $category = null)
    {
        if ($category instanceof Category) {
            return $category->update($requestData);
        }

        return Category::query()->create($requestData);
    }

    public function delete(int $id): void
    {
        $model = $this->getById($id);

        if (!$model->delete()) {
            throw new \RuntimeException('Can\'t delete model ' . $model->name);
        }
    }

    public function getCategoriesForCurrentModel(int $modelId): Collection
    {
        return $this->repository->findAllByModelId($modelId);
    }
}