<?php

namespace App\Services;

use App\DTO\QueryParamsDTO;
use App\Product;
use App\Repositories\ProductRepository;

class ProductService
{
    use ServiceTrait;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(array $requestData, Product $product = null)
    {
        if ($product instanceof Product) {
            return $product->update($requestData);
        }

        return Product::query()->create($requestData);
    }

    public function delete(int $id): void
    {
        $model = $this->getById($id);

        if (!$model->delete()) {
            throw new \RuntimeException('Can\'t delete model ' . $model->name);
        }
    }

    public function getAllByFiltersAndModel(int $modelId, QueryParamsDTO $dto)
    {
        return $this->repository->findAllByFiltersAndModel($modelId, $dto);
    }
}