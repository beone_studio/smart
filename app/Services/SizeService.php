<?php

namespace App\Services;

use App\Repositories\SizeRepository;
use App\Size;

class SizeService
{
    use ServiceTrait;

    public function __construct(SizeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(array $requestData, Size $size = null)
    {
        if ($size instanceof Size) {
            return $size->update($requestData);
        }

        return Size::query()->create($requestData);
    }

    public function delete(int $id): void
    {
        $model = $this->getById($id);

        if (!$model->delete()) {
            throw new \RuntimeException('Can\'t delete model ' . $model->name);
        }
    }
}