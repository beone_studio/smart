<?php

namespace App\Services;

use App\Promotion;
use App\Repositories\PromotionRepository;
use Illuminate\Http\Request;

class PromotionService
{
    use ServiceTrait;

    public function __construct(PromotionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(Request $request, Promotion $promotion = null)
    {
        $requestData = [];

        foreach(\json_decode($request->input('json')) as $key => $value) {
            $requestData[$key] = $value;
        }

        if ($promotion instanceof Promotion) {
            if ($request->hasFile('image')) {
                \Storage::disk('public')->delete($promotion->image);
                $requestData['image'] = $request->image->store('promotions', 'public');
            }

            return $promotion->update($requestData);
        }

        $requestData['image'] = $request
            ->file('image')
            ->store('promotions', 'public');

        return Promotion::query()->create($requestData);
    }

    public function delete(int $id): void
    {
        $model = $this->getById($id);

        if (!$model->delete()) {
            throw new \RuntimeException('Can\'t delete model ' . $model->name);
        }
    }
}