<?php

namespace App\Services;

use App\Repositories\RepositoryTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait ServiceTrait
{
    /**
     * @var RepositoryTrait
     */
    protected $repository;

    public function getAll(): Collection
    {
        return $this->repository->findAll();
    }

    public function getAllOrderByOrder(): Collection
    {
        return $this->repository->findAllOrderByOrder();
    }

    public function getById(int $id): ?Model
    {
        return $this->repository->findById($id);
    }
}