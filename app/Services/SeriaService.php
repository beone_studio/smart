<?php

namespace App\Services;

use App\Category;
use App\Repositories\SeriaRepository;
use App\Series;
use Illuminate\Http\Request;

class SeriaService
{
    use ServiceTrait;

    public function __construct(SeriaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(Request $request, Series $seria = null)
    {
        $requestData = [];

        foreach(\json_decode($request->input('json')) as $key => $value) {
            $requestData[$key] = $value;
        }

        if ($seria instanceof Series) {
            if ($request->hasFile('image')) {
                \Storage::disk('public')->delete($seria->image);
                $requestData['image'] = $request->image->store('series', 'public');
            }

            return $seria->update($requestData);
        }

        $requestData['image'] = $request
            ->file('image')
            ->store('series', 'public');

        return Series::query()->create($requestData);
    }

    public function delete(int $id): void
    {
        $model = $this->getById($id);

        if (!$model->delete()) {
            throw new \RuntimeException('Can\'t delete model ' . $model->name);
        }
    }

    public function attachCategory(Series $seria, Request $request): void
    {
        $categoriesIds = explode(',',$request->input('categories'));
        $category = Category::query()->find($categoriesIds);
        $seria->categories()->detach();
        $seria->categories()->attach($category);
    }

    public function detachCategory(Series $seria, Category $category): void
    {
        $seria->categories()->detach($category);
    }
}