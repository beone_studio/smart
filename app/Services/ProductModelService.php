<?php

namespace App\Services;

use App\ProductModel;
use App\Repositories\ProductModelRepository;

class ProductModelService
{
    use ServiceTrait;

    public function __construct(ProductModelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(array $requestData, ProductModel $size = null)
    {
        if ($size instanceof ProductModel) {
            return $size->update($requestData);
        }

        return ProductModel::query()->create($requestData);
    }

    public function delete(int $id): void
    {
        $model = $this->getById($id);

        if (!$model->delete()) {
            throw new \RuntimeException('Can\'t delete model ' . $model->name);
        }
    }
}