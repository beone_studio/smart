<?php

namespace App\Repositories;

use App\Series;
use Illuminate\Database\Eloquent\Model;

class SeriaRepository
{
    use RepositoryTrait;

    public function __construct(string $modelClass = Series::class)
    {
        $this->modelClass = $modelClass;
    }

    public function findById(int $id): Model
    {
        return Series::query()
            ->with('categories')
            ->find($id);
    }
}