<?php

namespace App\Repositories;

use App\CaseType;

class CaseTypeRepository
{
    use RepositoryTrait;

    public function __construct(string $modelClass = CaseType::class)
    {
        $this->modelClass = $modelClass;
    }
}