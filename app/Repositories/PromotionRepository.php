<?php

namespace App\Repositories;

use App\Promotion;

class PromotionRepository
{
    use RepositoryTrait;

    public function __construct(string $modelClass = Promotion::class)
    {
        $this->modelClass = $modelClass;
    }
}