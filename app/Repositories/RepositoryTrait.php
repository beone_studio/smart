<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait RepositoryTrait
{
    /**
     * @var Model
     */
    protected $modelClass;

    public function findById(int $id): ?Model
    {
        return $this->modelClass::query()->find($id);
    }

    public function findAll(): Collection
    {
        return $this->modelClass::query()
            ->orderBy('created_at', SORT_DESC)
            ->get();
    }

    public function findAllOrderByOrder(): Collection
    {
        return $this->modelClass::query()
            ->orderBy('order')
            ->get();
    }
}
