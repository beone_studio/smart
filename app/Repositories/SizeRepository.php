<?php

namespace App\Repositories;

use App\Size;

class SizeRepository
{
    use RepositoryTrait;

    public function __construct(string $modelClass = Size::class)
    {
        $this->modelClass = $modelClass;
    }
}