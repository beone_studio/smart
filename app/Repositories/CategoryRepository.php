<?php

namespace App\Repositories;

use App\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository
{
    use RepositoryTrait;

    public function __construct(string $modelClass = Category::class)
    {
        $this->modelClass = $modelClass;
    }

    public function findAllByModelId(int $modelId): Collection
    {
        return Category::query()
            ->where('model_id', $modelId)
            ->orderBy('order')
            ->get();
    }
}