<?php

namespace App\Repositories;

use App\ProductModel;

class ProductModelRepository
{
    use RepositoryTrait;

    public function __construct(string $modelClass = ProductModel::class)
    {
        $this->modelClass = $modelClass;
    }
}