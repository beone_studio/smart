<?php

namespace App\Repositories;

use App\DTO\QueryParamsDTO;
use App\Product;
use App\Series;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository
{
    use RepositoryTrait;

    public function __construct(string $modelClass = Product::class)
    {
        $this->modelClass = $modelClass;
    }

    public function findAll(): Collection
    {
        return Product::query()
            ->with([
                'size',
                'caseType',
                'series',
            ])
            ->get();
    }

    public function findAllByFiltersAndModel(int $modelId, QueryParamsDTO $dto): Collection
    {
        $query = Product::query()
            ->with([
                'series',
                'size',
                'caseType',
            ])
            ->select(Product::tableName() . '.*')
            ->join(
                Series::tableName(),
                Series::tableName() . '.id',
                '=',
                Product::tableName() . '.series_id'
            )
            ->where(
                Series::tableName() . '.model_id',
                '=',
                $modelId
            )
            ->orderBy('order')
            ->groupBy(Product::tableName() . '.id');

        if ($dto->getCategoryId()) {
            $query
                ->join(
                    'category_series',
                    'category_series.series_id',
                    '=',
                    Series::tableName() . '.id'
                )
                ->where(
                    'category_series.category_id',
                    '=',
                    $dto->getCategoryId()
                );
        }

        if ($dto->getSizeId()) {
            $query->where(
                Product::tableName() . '.size_id',
                '=',
                $dto->getSizeId()
            );
        }

        if ($dto->getCaseTypeId()) {
            $query->where(
                Product::tableName() . '.case_type_id',
                '=',
                $dto->getCaseTypeId()
            );
        }

        return $query->get();
    }
}