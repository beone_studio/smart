<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property int $id
 */
class Size extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'order',
    ];

    public function products()
    {
        return $this->hasMany(
            Product::class,
            'size_id',
            'id'
        );
    }
}
