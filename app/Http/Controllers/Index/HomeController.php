<?php

namespace App\Http\Controllers\Index;

use App\DTO\QueryParamsDTO;
use App\Http\Controllers\Controller;
use App;
use App\Services\CaseTypeService;
use App\Services\CategoryService;
use App\Services\ProductModelService;
use App\Services\ProductService;
use App\Services\SizeService;
use App\Services\PromotionService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $productModelService;
    private $categoryService;
    private $productService;
    private $sizeService;
    private $caseTypeService;
    private $promotionService;

    public function __construct(
        ProductModelService $productModelService,
        CategoryService $categoryService,
        ProductService $productService,
        SizeService $sizeService,
        CaseTypeService $caseTypeService,
        PromotionService $PromotionService)
    {
        $this->productModelService = $productModelService;
        $this->categoryService = $categoryService;
        $this->productService = $productService;
        $this->sizeService = $sizeService;
        $this->caseTypeService = $caseTypeService;
        $this->promotionService = $PromotionService;
    }

    public function index()
    {
        $models = $this->productModelService->getAllOrderByOrder();
        $sizes = $this->sizeService->getAllOrderByOrder();
        $promotions = $this->promotionService->getAll();
        $caseTypes = [];

        $caseType = $this->caseTypeService->getById(46);

        if ($caseType instanceof App\CaseType) {
            $caseTypes[] = $caseType;
        }

        $caseType = $this->caseTypeService->getById(47);

        if ($caseType instanceof App\CaseType) {
            $caseTypes[] = $caseType;
        }

        return \view('index.main')
            ->with(compact('models'))
            ->with(compact('caseTypes'))
            ->with(compact('sizes'))
            ->with(compact('promotions'));
    }

    public function getCategories(int $modelId): Collection
    {
        return $this->categoryService->getCategoriesForCurrentModel($modelId);
    }

    public function getProducts(int $modelId, Request $request)
    {
        $categoryId = $request->query('category');
        $sizeId = $request->query('size');
        $caseTypeId = $request->query('caseType');
        $dto = new QueryParamsDTO($categoryId, $sizeId, $caseTypeId);

        return $this->productService->getAllByFiltersAndModel($modelId, $dto);
    }
}
