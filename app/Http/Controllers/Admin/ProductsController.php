<?php

namespace App\Http\Controllers\Admin;

use App\Product;

use App\Services\ProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function getAllProducts()
    {
        return $this->service->getAll();
    }

    public function getProduct($id)
    {
        return $this->service->getById($id);
    }

    public function create(Request $request)
    {
        $this->service->save($request->all());

        return ['result' => 'success'];
    }

    public function update(Product $product, Request $request)
    {
        $this->service->save($request->all(), $product);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return ['result' => 'success'];
    }
}