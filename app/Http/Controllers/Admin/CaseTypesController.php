<?php

namespace App\Http\Controllers\Admin;

use App\CaseType;

use App\Services\CaseTypeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CaseTypesController extends Controller
{
    private $service;

    public function __construct(CaseTypeService $service)
    {
        $this->service = $service;
    }

    public function getAllCaseTypes()
    {
        return $this->service->getAll();
    }

    public function getCaseType($id)
    {
        return $this->service->getById($id);
    }

    public function create(Request $request)
    {
        $this->service->save($request->all());

        return ['result' => 'success'];
    }

    public function update(CaseType $case, Request $request)
    {
        $this->service->save($request->all(), $case);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return ['result' => 'success'];
    }
}