<?php

namespace App\Http\Controllers\Admin;

use App\ProductModel;

use App\Services\CategoryService;
use App\Services\ProductModelService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductModelsController extends Controller
{
    private $service;
    private $categoryService;

    public function __construct(ProductModelService $service, CategoryService $categoryService)
    {
        $this->service = $service;
        $this->categoryService = $categoryService;
    }

    public function getAllModels()
    {
        return $this->service->getAll();
    }

    public function getModel($id)
    {
        return $this->service->getById($id);
    }

    public function create(Request $request)
    {
        $this->service->save($request->all());

        return ['result' => 'success'];
    }

    public function update(ProductModel $model, Request $request)
    {
        $this->service->save($request->all(), $model);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return ['result' => 'success'];
    }

    public function getCategories(ProductModel $model)
    {
        return $this->categoryService->getCategoriesForCurrentModel($model->id);
    }
}