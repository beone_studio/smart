<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Promotion;
use App\Services\PromotionService;
use Illuminate\Http\Request;

class PromotionsController extends Controller
{
    private $service;

    public function __construct(PromotionService $service)
    {
        $this->service = $service;
    }

    public function getAll()
    {
        return $this->service->getAll();
    }

    public function getOne($id)
    {
        return $this->service->getById($id);
    }

    public function create(Request $request)
    {
        $this->service->save($request);

        return ['result' => 'success'];
    }

    public function update(Promotion $promotion, Request $request)
    {
        $this->service->save($request, $promotion);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return ['result' => 'success'];
    }
}