<?php

namespace App\Http\Controllers\Admin;

use App\Services\SizeService;
use App\Size;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SizesController extends Controller
{
    private $service;

    public function __construct(SizeService $service)
    {
        $this->service = $service;
    }

    public function getAllSizes()
    {
        $sizes = $this->service->getAllOrderByOrder()->toArray();

        \usort($sizes, function ($size1, $size2) {
            return \strnatcmp($size1['name'], $size2['name']);
        });

        return $sizes;
    }

    public function getSize($id)
    {
        return $this->service->getById($id);
    }

    public function create(Request $request)
    {
        $this->service->save($request->all());

        return ['result' => 'success'];
    }

    public function update(Size $size, Request $request)
    {
        $this->service->save($request->all(), $size);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return ['result' => 'success'];
    }
}