<?php

namespace App\Http\Controllers\Admin;

use App\Category;

use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    private $service;

    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }

    public function getAllCategories()
    {
        return $this->service->getAll();
    }

    public function getCategory($id)
    {
        return $this->service->getById($id);
    }

    public function create(Request $request)
    {
        $this->service->save($request->all());

        return ['result' => 'success'];
    }

    public function update(Category $category, Request $request)
    {
        $this->service->save($request->all(), $category);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return ['result' => 'success'];
    }
}
