<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Series;
use App\Services\SeriaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeriesController extends Controller
{
    private $service;

    public function __construct(SeriaService $service)
    {
        $this->service = $service;
    }

    public function getAllSeries()
    {
        return $this->service->getAll();
    }

    public function getSeria($id)
    {
        return $this->service->getById($id);
    }

    public function create(Request $request)
    {
        $this->service->save($request);

        return ['result' => 'success'];
    }

    public function update(Series $seria, Request $request)
    {
        $this->service->save($request, $seria);

        return ['result' => 'success'];
    }

    public function attachCategories(Series $seria, Request $request)
    {
        $this->service->attachCategory($seria, $request);

        return ['result' => 'success'];
    }

    public function detachCategory(Series $seria, Category $category)
    {
        $this->service->detachCategory($seria, $category);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return ['result' => 'success'];
    }
}