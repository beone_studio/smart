<?php

namespace App\Http\Controllers;

use App\CaseType;
use App\Product;
use App\ProductModel;
use App\Series;
use App\Size;

class TestController extends Controller
{
    //TODO: for test model
    public function test()
    {
        $productId = 1;
        $matrasObrigani = Product::findOrFail($productId);
        $size = $matrasObrigani->size;
        $caseType = $matrasObrigani->caseType;
        $series = $matrasObrigani->series;
        $categories = $series->categories;
        $productModel = $series->productModel;

        var_dump($matrasObrigani);
        die;
    }
}