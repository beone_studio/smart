<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use App\Services\SizeService;
use App\Services\SeriaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    private $productService;
    private $sizeService;
    private $seriaService;

    public function __construct(ProductService $productService, SizeService $sizeService, SeriaService $seriaService)
    {
        $this->productService = $productService;
        $this->sizeService = $sizeService;
        $this->seriaService = $seriaService;
    }

    public function send(Request $request)
    {
        $productId = $request->input('product');
        $user = $request->input('user');
        $telephone = $request->input('telephone');
        $size_name = null;
        $product_name = null;

        if ($productId && $productId != '-1') {
            $product = $this->productService->getById($productId);
            $size = $this->sizeService->getById($product->size_id);
            $seria = $this->seriaService->getById($product->series_id);
            $product_name = $seria->name;
            $size_name = $size->name;
        } else if ($productId == '-1') {
            $product_name = "скачку файла";
        }


        $title = $product_name ? ('Заявка на ' . $product_name) : 'Smart-choice | Новая заявка';

        Mail::send(
            'emails.order',
            [
                'title' => $title,
                'name' => $user,
                'number' => $telephone,
                'size' => $size_name
            ],
            function ($message) use ($title) {

                $message->from('baraulia@yandex.ru', $title);

                $message->to('baraulia@yandex.ru')->subject($title);

            }
        );

        return ($productId && $productId != '-1') ? ['result' => 'success'] : redirect('/');

    }
}
