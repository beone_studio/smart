<?php

namespace App\DTO;

class QueryParamsDTO
{
    private $categoryId;
    private $sizeId;
    private $caseTypeId;

    public function __construct(
        ?int $categoryId,
        ?int $sizeId,
        ?int $caseTypeId)
    {
        $this->categoryId = $categoryId;
        $this->sizeId = $sizeId;
        $this->caseTypeId = $caseTypeId;
    }

    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    public function getSizeId(): ?int
    {
        return $this->sizeId;
    }

    public function getCaseTypeId(): ?int
    {
        return $this->caseTypeId;
    }
}