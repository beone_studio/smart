<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 */
class Category extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'order',
        'model_id',
    ];

    public static function tableName(): string
    {
        return 'categories';
    }

    public function model()
    {
        return $this->belongsTo(
            ProductModel::class,
            'model_id',
            'id'
        );
    }

    public function series()
    {
        return $this->belongsToMany(
            Series::class,
            'category_series',
            'category_id',
            'series_id'
        );
    }
}
