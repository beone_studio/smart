<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 */
class CaseType extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'order',
    ];

    public function products()
    {
        return $this->hasMany(
            Product::class,
            'case_type_id',
            'id'
        );
    }
}
