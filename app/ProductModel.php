<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class ProductModel extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'order',
        'description',
    ];

    public function series()
    {
        return $this->hasMany(
            Series::class,
            'model_id',
            'id'
        );
    }

    public function categories()
    {
        return $this->hasMany(
            Category::class,
            'model_id',
            'id'
        );
    }
}
