<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewCol extends Migration
{
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('model_id')->unsigned();

            $table
                ->foreign('model_id')
                ->references('id')
                ->on('product_models')
                ->onDelete('CASCADE');
        });
    }

    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign(['model_id']);

            $table->dropColumn('model_id');
        });
    }
}
