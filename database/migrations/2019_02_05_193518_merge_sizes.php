<?php

use App\Product;
use App\Size;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MergeSizes extends Migration
{
    private const MERGED_SIZES = [
        '80х190, 195, 200' => '80х186, 80х190, 80х195, 80х200',
        '158х198' => '158х190, 195, 198, 200',
    ];

    public function up()
    {
        Schema::table('sizes', function (Blueprint $table) {
            $table->unique('name', 'unique_name');
        });

        $this->reassignSizes();
    }

    public function down()
    {
        Schema::table('sizes', function (Blueprint $table) {
            $table->dropUnique('unique_name');
        });
    }

    private function getSizeByName(string $name): ?Size
    {
        return Size::query()->where('name', 'like', $name)->first();
    }

    private function reassignSizes(): void
    {
        foreach (self::MERGED_SIZES as $old => $new) {
            $oldSize = $this->getSizeByName($old);
            $newSize = $this->getSizeByName($new);

            if ($oldSize instanceof Size && $newSize instanceof Size) {
                /** @var Product $product */
                foreach ($oldSize->products()->get() as $product) {
                    $product->size()->associate($newSize)->save();
                }

                if ($oldSize->products()->count() === 0) {
                    $oldSize->delete();
                }
            }
        }
    }
}
