<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('size_id')->unsigned();
            $table->integer('series_id')->unsigned();
            $table->integer('case_type_id')->unsigned();
            $table->integer('order')->nullable();
            $table->timestamps();

            $table->foreign('size_id')->references('id')->on('sizes')->onDelete('CASCADE');
            $table->foreign('series_id')->references('id')->on('series')->onDelete('CASCADE');
            $table->foreign('case_type_id')->references('id')->on('case_types')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
