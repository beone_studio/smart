<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            $table->integer('weight')->nullable();
            $table->integer('weight_2')->nullable();
            $table->integer('feel_1')->nullable();
            $table->integer('feel_2')->nullable();
            $table->integer('feel_3')->nullable();
            $table->integer('feel_4')->nullable();
            $table->integer('height')->nullable();
            $table->string('name')->index();
            $table->string('slug')->nullable();
            $table->integer('order')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('model_id')->references('id')->on('product_models')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
