<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/send', 'EmailController@send');


//TODO: delete me after test fetching data from product model
Route::get('/test', 'TestController@test');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AuthController@login');
    Route::post('/', 'AuthController@doLogin');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::get('/logout', 'AuthController@logout');
            Route::post('/features', 'CategoriesController@createFeature');
            Route::get('/allCategories', 'CategoriesController@getAllCategories');
            Route::delete('/features/{id}', 'CategoriesController@deleteFeature');
            Route::get('/allProjectTypes', 'ProjectTypesController@getAllTypes');

            Route::group(['prefix' => 'categories'], function () {
                Route::get('/', 'CategoriesController@getAllCategories');
                Route::get('/{category}', 'CategoriesController@getCategory');
                Route::delete('/{id}', 'CategoriesController@delete');
                Route::post('/', 'CategoriesController@create');
                Route::post('/{category}', 'CategoriesController@update');
            });

            Route::group(['prefix' => 'sizes'], function () {
                Route::get('/', 'SizesController@getAllSizes');
                Route::get('/{size}', 'SizesController@getSize');
                Route::delete('/{id}', 'SizesController@delete');
                Route::post('/', 'SizesController@create');
                Route::post('/{size}', 'SizesController@update');
            });

            Route::group(['prefix' => 'models'], function () {
                Route::get('/', 'ProductModelsController@getAllModels');
                Route::get('/{model}', 'ProductModelsController@getModel');
                Route::delete('/{id}', 'ProductModelsController@delete');
                Route::post('/', 'ProductModelsController@create');
                Route::post('/{model}', 'ProductModelsController@update');
                Route::get('/{model}/categories', 'ProductModelsController@getCategories');
            });

            Route::group(['prefix' => 'cases'], function () {
                Route::get('/', 'CaseTypesController@getAllCaseTypes');
                Route::get('/{case}', 'CaseTypesController@getCaseType');
                Route::delete('/{case}', 'CaseTypesController@delete');
                Route::post('/', 'CaseTypesController@create');
                Route::post('/{case}', 'CaseTypesController@update');
            });

            Route::group(['prefix' => 'series'], function () {
                Route::get('/', 'SeriesController@getAllSeries');
                Route::get('/{seria}', 'SeriesController@getSeria');
                Route::delete('/{seria}', 'SeriesController@delete');
                Route::post('/', 'SeriesController@create');
                Route::post('/{seria}', 'SeriesController@update');
                Route::post('/{seria}/categories', 'SeriesController@attachCategories');
                Route::delete('/{seria}/categories/{category}', 'SeriesController@detachCategory');
            });

            Route::group(['prefix' => 'products'], function () {
                Route::get('/', 'ProductsController@getAllProducts');
                Route::get('/{product}', 'ProductsController@getProduct');
                Route::delete('/{product}', 'ProductsController@delete');
                Route::post('/', 'ProductsController@create');
                Route::post('/{product}', 'ProductsController@update');
            });

            Route::group(['prefix' => 'promotions'], function () {
                Route::get('/', 'PromotionsController@getAll');
                Route::get('/{promotion}', 'PromotionsController@getOne');
                Route::delete('/{promotion}', 'PromotionsController@delete');
                Route::post('/', 'PromotionsController@create');
                Route::post('/{promotion}', 'PromotionsController@update');
            });
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});

Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/categories/{model}', 'HomeController@getCategories');
    Route::get('/products/{model}', 'HomeController@getProducts');
});

